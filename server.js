const url = require('url');
const fs = require('fs');
const uuid = require("uuid");

var host = '0.0.0.0';
var port = 3000;
const http = require('http').createServer();
const io = require('socket.io')(http, {
    cors: {
        origin: `*`,
        methods: ["GET", "POST"]
    }
});
// const server = require('express')();
// const http = require('http').createServer(server);

var onlineUser = [];
var joiningRoomUser = [];
var gameRoom = new Map();

io.on('connection', function (socket) {
    OnConnection(socket);
});

http.listen(port, host, {perMessageDeflate: false}, function () {
    console.log('Server started! Listening on: http://' + host + ':' + port);
});

function OnConnection(socket) {
    console.log('User connected: ' + socket.id);

    socket.emit('request register');

    //  Register function
    socket.once('register', (data) => {
        if (data?.token) {
            //  Register user with token and replace with new id
            var result = registerWithToken(data.token, socket);
            if (!result) {
                // Register failed
                socket.emit('register result', {
                    'result': 'failed',
                    'reason': 'Invalid token. Please rejoin the game from the room list.',
                });
                console.log('User register failed: ' + socket.id);
                socket.disconnect();
                return;
            }

            //  Register success
            var roomName = getRoomNameByUserId(socket.id);
            if (roomName) {
                socket.join(roomName);
                socket.emit('register result', {
                    'result': 'success',
                    'reason': 'OK',
                    'name': result.name,
                    'roomName': result.roomName,
                    'isPlayerA': (gameRoom.get(roomName).length === 1),
                    'users': gameRoom.get(roomName)
                });

                //  Bind function
                bindEvent(socket);
            } else {
                // Cannot find joined room
                socket.emit('register result', {
                    'result': 'failed',
                    'reason': 'Cannot find joined room. Please rejoin the game from the room list.',
                });
                console.log('Cannot find joined room: ' + socket.id);
                socket.disconnect();
                return;
            }
        } else {
            // Register new user
            if (data?.name) {
                if (!canUserRegister(data.name)) {
                    socket.emit('register result', {
                        'result': 'failed',
                        'reason': 'Invalid user name or user name has been used.',
                    });
                    console.log('User register failed: ' + socket.id);
                    socket.disconnect();
                    return;
                }
            } else {
                socket.emit('register result', {
                    'result': 'failed',
                    'reason': 'Please join the game from the room list.',
                });
                console.log('User register failed: ' + socket.id);
                socket.disconnect();
            }

            socket.data.name = data.name;
            onlineUser.push({
                'id': socket.id,
                'name': data.name,
            });
            socket.emit('register result', {
                'result': 'success',
                'reason': 'OK',
            });
            console.log('User register success: Id:' + socket.id + ' Name:' + socket.data.name);

            //  Bind function
            bindEvent(socket);

            notifyRoomStatusChanged();
        }
    });

    socket.on('disconnect', () => {
        onDisconnect(socket);
    });
}

function bindEvent(socket) {
    socket.on('heatbeat', function (data) {
        onHeatbeat(socket, data);
    });
    socket.on('join room', function (data) {
        onJoinRoom(socket, data);
        io.to(data.roomName).emit('joinedUsers', gameRoom.get(data.roomName));
    });
    socket.on('leave room', function (data) {
        onLeaveRoom(socket, data);
    });

    socket.on('startGame', function (user, deck) {
        var roomName = getRoomNameByUserId(socket.id);
        if (roomName) {
            io.to(roomName).emit('startGame', user, deck);
        }
    });

    socket.on('drawCard', function (user, num) {
        var roomName = getRoomNameByUserId(socket.id);
        if (roomName) {
            io.to(roomName).emit('drawCard', user, num);
        }
    });

    socket.on('vote', function (user, zoneName) {
        var roomName = getRoomNameByUserId(socket.id);
        if (roomName) {
            io.to(roomName).emit('vote', user, zoneName);
        }
    });

    socket.on('cardPlayed', function (gameObject, zoneName, user) {
        var roomName = getRoomNameByUserId(socket.id);
        if (roomName) {
            io.to(roomName).emit('cardPlayed', gameObject, zoneName, user);
        }
    });

    socket.on('action', function (actionName, obj) {
        var roomName = getRoomNameByUserId(socket.id);
        if (roomName) {
            io.to(roomName).emit('action', actionName, obj);
        }
    });

}

function onHeatbeat(socket) {
    socket.emit('heatbeat', { 'date': new Date() });
}

function onDisconnect(socket) {
    var user = onlineUser.find(x => x.id == socket.id);
    if (user) {
        onlineUser.splice(onlineUser.indexOf(user), 1);
        if (user.token) {
            joiningRoomUser.push(user);
        }
    }
    if (!(user?.token)) {
        gameRoom.forEach((value, key) => {
            removeGameRoomUser(key, socket);
        });
    }

    console.log('User disconnected: ' + socket.id + ' ' + socket.data.name);
    notifyRoomStatusChanged();
}

function onJoinRoom(socket, data) {
    if (!isValidString(data?.roomName)) {
        socket.emit('join room result', {
            'result': 'failed',
            'reason': 'Invalid room name. Only alphanumeric,_ and space is allowed.',
        });
        return false;
    }
    if (isUserInGameRoom(data.roomName, socket.id)) {
        socket.emit('join room result', {
            'result': 'failed',
            'reason': 'You are already inside the room',
        });
        return false;
    }
    var room = gameRoom.get(data.roomName);
    if (room && room.length >= 6) {
        socket.emit('join room result', {
            'result': 'failed',
            'reason': 'Room is full.',
        });
        return false;
    }

    socket.join(data.roomName);
    var user = addGameRoomUser(data.roomName, socket,);
    socket.emit('join room result', {
        'result': 'success',
        'token': user.token,
    });
    console.log('join room result: ' + user.id + ' ' + user.token);
    socket.to(data.roomName).emit('user join room', {
        'id': socket.id,
        'name': socket.data.name,
    });
    console.log(socket.id + ' joined ' + data.roomName);
    notifyRoomStatusChanged();
}

function onLeaveRoom(socket, data) {
    socket.leave(data.roomName);
    removeGameRoomUser(data.roomName, socket);
    socket.to(data.roomName).emit('user join room', { 'id': socket.id, 'name': socket.data.name });
    console.log(socket.id + ' leave ' + data.roomName);
    notifyRoomStatusChanged();
}

function canUserRegister(name) {
    if (!name) return false;
    if (name.trim().length <= 0) return false;
    if (!isValidString(name)) return false;
    return !onlineUser.some(function (user) {
        return user.name == name;
    });
}

function addGameRoomUser(roomName, socket) {
    var room = gameRoom.get(roomName);
    if (!room) {
        room = [];
        gameRoom.set(roomName, room);
    }

    var user = room.find(x => x.id == socket.id);
    if (!user) {
        user = onlineUser.find(x => x.id == socket.id);
        if (user) {
            user.token = uuid.v4();
            room.push(user);
            return user;
        }
    } else {
        return user;
    }
}

function removeGameRoomUser(roomName, socket) {
    var room = gameRoom.get(roomName);
    if (!room) return;

    var user = room.find(x => x.id == socket.id);
    if (user) {
        room.splice(room.indexOf(user), 1);
        if (room.length == 0) {
            gameRoom.delete(roomName);
        }
    }
}

function getAllGameRooms() {
    var rooms = [];
    gameRoom.forEach((value, key, map) => {
        var users = [];
        value.forEach(m => {
            users.push(m.name);
        });
        rooms.push(
            {
                "name": key,
                "users": users,
            });
    });

    return rooms;
}

function notifyRoomStatusChanged() {
    io.emit('room list', { 'rooms': getAllGameRooms() });
}

function isUserInGameRoom(roomName, id) {
    var room = gameRoom.get(roomName);
    if (room) {
        return room.some(function (user) {
            return user.id == id;
        });
    }
    return false;
}

function registerWithToken(token, socket) {
    var returnVal;
    var user = joiningRoomUser.find(x => x.token == token);
    if (user) {
        user.id = socket.id;
        user.token = undefined;
        joiningRoomUser.splice(joiningRoomUser.indexOf(user), 1);
        onlineUser.push(user);

        gameRoom.forEach((value, key) => {
            var room = gameRoom.get(key);
            var user = room.find(x => x.id == socket.id);
            if (user) {
                returnVal = {
                    'name': user.name,
                    'roomName': key,
                };
                console.log('Register with token: ' + JSON.stringify(returnVal));
                return;
            }
        });

        return returnVal;
    }
}

function getRoomNameByUserId(id) {
    var roomName;
    gameRoom.forEach((value, key, map) => {
        if (isUserInGameRoom(key, id)) {
            roomName = key;
            return;
        }
    });
    return roomName;
}

function isValidString(str) {
    if (str) {
        var regex = /^[\w\s]+$/;
        if (str.match(regex))
            return true;
    }
    return false;
}